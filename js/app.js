'use strict';

var bookCatalog = angular.module('bookCatalog', ['ngRoute', 'ngSanitize', 'ngDragDrop']);
var apiBaseUrl = 'http://university.netology.ru/api/';

bookCatalog.config(function($routeProvider, $locationProvider) {
  $routeProvider
    .when('/', {
      templateUrl : 'template/list-book/tmp.html',
      controller  : 'ListBookController'
    })
    .when('/book/:id', {
      templateUrl : 'template/show-book/tmp.html',
      controller  : 'ShowBookController'
    })
    .when('/company', {
      templateUrl : 'template/shop-info/tmp.html',
      controller  : 'ShopInfoController'
    })
    .when('/buy/:id', {
      templateUrl : 'template/get-book/tmp.html',
      controller  : 'GetBookController'
    })
    .otherwise({
      redirectTo: '/'
    });
});

bookCatalog.filter('getPrice', function() {
  return function(price, input, currentCharCode, needCharCode) {
    var i;
    var codeCurrentData;
    var codeNeedleData;
    var priceReturn;
    console.log("input.length");
    for (i=0; i< input.length; i++) {
      if (input[i].ID == currentCharCode) {
        codeCurrentData = input[i];
      }

      if (input[i].ID == needCharCode) {
        codeNeedleData = input[i];
      }
    }

    priceReturn = price * (codeCurrentData.Value / codeCurrentData.Nominal) / (codeNeedleData.Value / codeNeedleData.Nominal);
    return Math.round(priceReturn);
  }
});


bookCatalog.controller('GetBookController', function ($scope, $http, $routeParams, $sce, $filter) {
  $scope.pageTitle =  "Купить книгу";
  $scope.formData = { showAddr: false }

  $scope.send = function () {
    $scope.formParams = {};
    $scope.formParams.manager = "info@webinface.ru";
    $scope.formParams.book = $scope.bookData.id;
    $scope.formParams.name = $scope.formData.name;
    $scope.formParams.phone = $scope.formData.phone;
    $scope.formParams.email = $scope.formData.email;
    $scope.formParams.comment = $scope.formData.comments;
    $scope.formParams.delivery = { id: $scope.formData.deliveryId };
    if ($scope.formData.showAddr == true) $scope.formParams.delivery.address = $scope.formData.addr;
    $scope.formParams.payment = { id: $scope.formData.paymentId, currency: $scope.formData.currency };

    // Отправить данные
    $http({
      method: 'POST',
      url: apiBaseUrl + 'order',
      data: $scope.formParams,
      cache: false
    })
      .then(function successCallback(response) {
        console.log(response);
      }, function errorCallback(response) {
        console.log(response);
      });
  }

  $scope.getDeliveryPrice = function (item) {
    return $filter('getPrice')(item.price, $scope.currencyData, item.currency, $scope.currentMoneyCode);
  }

  $scope.setAddr = function (delivery) {
    $scope.formData.deliveryId = delivery.id;
    $scope.formData.deliveryName = delivery.name;
    $scope.formData.deliveryPrice = $filter('getPrice')(delivery.price, $scope.currencyData, delivery.currency, $scope.currentMoneyCode);

    if (delivery.needAdress) {
      $scope.formData.showAddr = true;
    } else $scope.formData.showAddr = false;
  };

  $scope.setPayment = function (payment) {
    console.log(payment);
    $scope.formData.paymentId = payment.id;
    $scope.formData.currency = $scope.currentMoneyCode;
  }

  // Получить информацию о книге
  $http({
    method: 'GET',
    url: apiBaseUrl + 'book/' + $routeParams.id,
    cache: false
  })
    .then(function successCallback(response) {
      $scope.bookData = response.data;
    }, function errorCallback(response) {
      console.log(response);
    });

    // Получить список валют
    $http({
      method: 'GET',
      url: apiBaseUrl + 'currency/',
      cache: false
    })
      .then(function successCallback(response) {
        $scope.currencyData = response.data;
        $scope.currentMoneyCode = 'R01589';
        $scope.price = $filter('getPrice')($scope.bookData.price, $scope.currencyData, $scope.bookData.currency, $scope.currentMoneyCode);
      }, function errorCallback(response) {
        console.log(response);
      });

      // Получить способы доставки
      $http({
        method: 'GET',
        url: apiBaseUrl + 'order/delivery',
        cache: false
      })
        .then(function successCallback(response) {
          $scope.deliveryData = response.data;
        }, function errorCallback(response) {
          console.log(response);
        });

        // Получить способы оплаты
        $http({
          method: 'GET',
          url: apiBaseUrl + 'order/payment',
          cache: false
        })
          .then(function successCallback(response) {
            $scope.paymentData = response.data;
          }, function errorCallback(response) {
            console.log(response);
          });
});

bookCatalog.controller('ListBookController', function ($scope, $http, $sce) {
  $scope.listAddClear = function (index) {
    if (index % 4 == 0) {
      return 'list-new-line';
    }
  }

  // Получить список книг
  $http({
    method: 'GET',
    url: apiBaseUrl + 'book',
    cache: false
  })
    .then(function successCallback(response) {
      $scope.books = response.data;
    }, function errorCallback(response) {
      console.log(response);
    });

  $scope.pageTitle =  "Самое незаметное издательство";
  $scope.listLimit = 8;
});

bookCatalog.controller('ShopInfoController', function ($scope) {
  $scope.pageTitle =  "Вы работаете - мы отдыхаем";
  console.log("its shopInfo controller");
});


bookCatalog.controller('ShowBookController', function ($scope, $routeParams, $http, $sce, $filter) {
  // Получить информацию о книге
  $http({
    method: 'GET',
    url: apiBaseUrl + 'book/' + $routeParams.id,
    cache: false
  })
    .then(function successCallback(response) {
      $scope.bookData = response.data;
    }, function errorCallback(response) {
      console.log(response);
    });

  // Получить список валют
  $http({
    method: 'GET',
    url: apiBaseUrl + 'currency/',
    cache: false
  })
    .then(function successCallback(response) {
      $scope.currencyData = response.data;
      $scope.currentMoneyCode = 'R01589';
      $scope.price = $filter('getPrice')($scope.bookData.price, $scope.currencyData, $scope.bookData.currency, $scope.currentMoneyCode);
    }, function errorCallback(response) {
      console.log(response);
    });
  $scope.pageTitle =  "Мы издаём книги - вы их можете не читать";
  console.log("its showBook controller");
});
